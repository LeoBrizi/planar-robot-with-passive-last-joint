# Planar Robot with Passive Last Joint

This project is for the underactuated robotics course. It has the aim of develop a method for trajectory planning and control a planar robots with a passive rotational last joint.
Slides of presentation: https://docs.google.com/presentation/d/1RQ2ZtN4FHPD35YnhDZETkdJnhKChy75JzNQyNJAib-w/edit?usp=sharing